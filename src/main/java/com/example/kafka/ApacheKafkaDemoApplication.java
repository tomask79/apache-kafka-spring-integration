package com.example.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
@ImportResource("applicationContext.xml")
public class ApacheKafkaDemoApplication {
        
    public static void main(String args[]) {
        SpringApplication.run(ApacheKafkaDemoApplication.class, args);
    }
}

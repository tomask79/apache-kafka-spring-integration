package com.example.kafka.runner;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component
@DependsOn(value="kafkaOutboundChannelAdapter")
public class MessageRunner implements CommandLineRunner {

	@Resource(name = "inputToKafka")
	private MessageChannel messageChannel;
	
	@Override
	public void run(String... args) throws Exception {
		for (String arg1 : args) {
			messageChannel.send(
					new GenericMessage<String>(arg1)
			);
		}
	}
}

# Using Apache Kafka with Spring Integration #

Apache Kafka is certainly most used JMS broker with distributed systems like Apache Hadoop for data ingress. Crucial features of Apache Kafka compared to other JMS brokers are (from my point of view):

* Apache Kafka is **stateless**, when you consume message from Kafka's topic, it's not removed. Kafka has explicit retain policy for published messages. So all of  your endpoints have to be idempotent.

* Apache Kafka is **strictly publish-subscribe** JMS broker. With Kafka you can only send messages to topics. **There are no queue artifacts**.

* In Apache Kafka, consumers are divided into consumer groups. Published messages are distributed into these consumer groups where **only one consumer per this group gets the message**.

* In Apache Kafka there are no queues, but if you have only one consumer group then you can get the effect of point to point messaging.

Let's see how to use Apache Kafka with **Spring Integration**. We will build simple demo of producing messages into Kafka's topic.

## Kafka installation and start up ##

On MacOS, simply use home-brew. It will install Apache Kafka with Apache ZooKeeper, which is used for syncing the data across the Apache Kafka cluster. 

**First you need to start Apache ZooKeeper**:

```
$KAFKA_HOME/bin/zookeeper-server-start.sh  $KAFKA_HOME/config/zookeeper.properties
```

When installing with home-brew you don't need to change any config. 

**To start Apache Kafka, simply run: **

```
$KAFKA_HOME/bin/kafka-server-start.sh  $KAFKA_HOME/config/server.properties
```

To start using Apache Kafka, let's create topic:

```
$KAFKA_HOME/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test_topic
```

To be sure that we successfully created previous topic, lets list all of topics in the Apache Kafka:

```
$KAFKA_HOME/bin/kafka-topics.sh --list --zookeeper localhost:2181
```

## Build Apache Kafka producer based on Spring Integration ##

Okay, we're gonna build simple Spring Integration application **publishing messages into Apache Kafka entered as program arguments**.

First, let's create Spring Integration infrastructure:


```
    <int:channel id="inputToKafka">
        <int:queue/>
    </int:channel>

    <int-kafka:outbound-channel-adapter
            id="kafkaOutboundChannelAdapter"
            kafka-producer-context-ref="kafkaProducerContext"
            channel="inputToKafka">
        <int:poller fixed-delay="1000" time-unit="MILLISECONDS" 
        receive-timeout="0" task-executor="taskExecutor"/>
    </int-kafka:outbound-channel-adapter>
    
    <task:executor id="taskExecutor" pool-size="5" 
     keep-alive="120" queue-capacity="500"/>
```

We this xml configuration, we created **queue channel** "inputToKafka" where we're going to be pushing messages. Bean with id "**kafkaOutboundChannelAdapter**" is an outbound channel adapter with defined asynchronous polling performing reading messages from "inputToKafka" channel and pushing them into Apache Kafka.

Now Apache Kafka producer configuration:
```
    <bean id="kafkaStringSerializer" 
    	  class="org.apache.kafka.common.serialization.StringSerializer" />

    <int-kafka:producer-context id="kafkaProducerContext">
        <int-kafka:producer-configurations>
            <int-kafka:producer-configuration 
                  broker-list="localhost:9092"
                  topic="test_topic"
                  key-class-type="java.lang.String"
                  value-class-type="java.lang.String"
                  key-serializer="kafkaStringSerializer"
                  value-serializer="kafkaStringSerializer"
                  />
        </int-kafka:producer-configurations>
    </int-kafka:producer-context>

```

Let's take a look at producer parameters:

```
#Apache Kafka broker cluster. Cluster where we're going to 
be publishing messages. Let's go with default configuration. 

broker-list="localhost:9092"
```

```
#Name of topic for publishing messages.
topic="test_topic"
```

```
#Type of the optional key associated with the message
key-class-type="java.lang.String"
```

```
#Type of value sent in the message.
value-class-type="java.lang.String"
```

```
#Reference to the key serializer, all keys 
and values has to be serialized before sending 
into Apache Kafka.

key-serializer="kafkaStringSerializer"
```

```
#Same and key serializer.
value-serializer="kafkaStringSerializer"
```

## Starting the message flow ##

To start the message flow we will simply create SpringBoot CommandLineRunner passing command line arguments into mentioned queue channel:


```
@Component
@DependsOn(value="kafkaOutboundChannelAdapter")
public class MessageRunner implements CommandLineRunner {

	@Resource(name = "inputToKafka")
	private MessageChannel messageChannel;
	
	@Override
	public void run(String... args) throws Exception {
		for (String arg1 : args) {
			messageChannel.send(
					new GenericMessage<String>(arg1)
			);
		}
	}
}
```  

## Howto test the application ##

In the root of maven application, run:

```
mvn clean install
```

after successfull compilation, run:

```
java -jar target/demo-0.0.1-SNAPSHOT.jar Test1 Test2 Test3
```
This will create String messages passed as arguments and push them into test_topic in our Apache Kafka broker.

Now if you run the Apache Kafka consumer:

```
$KAFKA_HOME/kafka-console-consumer.sh --zookeeper localhost:2181 --topic test_topic --from-beginning
```

You should get:

```
tomask79:bin tomask79$ kafka-console-consumer.sh --zookeeper localhost:2181 --topic test_topic --from-beginning
Test1
Test2
Test3
```

Great, we created simple Spring Integration based message producer into Apache Kafka!

There is certainly a lot to test with Apache Kafka. Especially topic offset keeping when reading messages and Apache Spark integration. Will come next!

cheers

Tomas